package org.teiath.ellak.ellakandroideducation;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import junit.framework.Test;

import java.util.LinkedList;

/**
 * Αποτελεί την αρχική οθόνη της εφαρμογής. Παρουσιάζει τα στοιχεία του project (ΕΛ/ΛΑΚ, τίτλος εφαρμογής, κλπ).
 * Καλό είναι να καταλαμβάνει όλη την οθόνη (να κρύβει την notification bar).
 * Ελέγχει αν υπάρχει update της βάσης και αν υπάρχει ρωτάει τον χρήστη αν θέλει να πραγματοποιηθεί και...
 * πράττει ανάλογα.
 * Η Splash Screen οδηγεί στην Option Screen.
 */
public class SplashScreen extends Activity
{
    TextView tv;
    public static Context context;
    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_splash);

        DBHandler dbHandler = DBHandler.getInstance(getApplicationContext());
        float b = dbHandler.GetVersion();
        tv = (TextView) findViewById(R.id.textView);
        tv.setText(String.valueOf(b));
        TestSheet ts = dbHandler.CreateTestSheet(2);
        LinkedList<SubjectRec> l = dbHandler.GetKategories();
        tv.setText(ts.Quests[1].AText[0]);
    }

    public static  Context getContext(){
        return context;
    }
}
